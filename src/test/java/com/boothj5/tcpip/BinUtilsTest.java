package com.boothj5.tcpip;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class BinUtilsTest {

    @Test
    public void returns8BitString() {
        assertEquals("00000000", BinUtils.intToBin(8, 0));
        assertEquals("00000001", BinUtils.intToBin(8, 1));
        assertEquals("00001101", BinUtils.intToBin(8, 13));
        assertEquals("11111111", BinUtils.intToBin(8, 255));
    }

    @Test
    public void returns16BitString() {
        assertEquals("0000000000000000", BinUtils.intToBin(16, 0));
        assertEquals("0000000000000001", BinUtils.intToBin(16, 1));
        assertEquals("0000000000001101", BinUtils.intToBin(16, 13));
        assertEquals("0000000011111111", BinUtils.intToBin(16, 255));

        assertEquals("1000000000000001", BinUtils.intToBin(16, 32769));
        assertEquals("1111000000001101", BinUtils.intToBin(16, 61453));
        assertEquals("1111111111111111", BinUtils.intToBin(16, 65535));
    }
}