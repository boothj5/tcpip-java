package com.boothj5.tcpip.address;

import com.boothj5.tcpip.exception.InvalidIpAddressException;
import com.boothj5.tcpip.exception.Ip4ComaptibleConversionException;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.fail;

public class Ip6AddressTest {

    private Ip6Address addr;

    @Test
    public void defaultsToZeroBlocks() {
        addr = new Ip6Address();
        assertEquals("::", addr.toAddressString());
    }

    @Test
    public void throwsExceptionWhenNull() {
        try {
            new Ip6Address(null);
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip6Address, cannot be null or empty String", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenEmpty() {
        try {
            new Ip6Address("");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip6Address, cannot be null or empty String", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenWhitespace() {
        try {
            new Ip6Address("  ");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip6Address, cannot be null or empty String", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenOneBlock() {
        try {
            new Ip6Address("123");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip6Address, invalid number of blocks", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenInvalidSeparator() {
        try {
            new Ip6Address("1F:A2,FF21:BCD4:AAAA:1234:RF32:1121");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip6Address, invalid number of blocks", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenTwoManyBlocks() {
        try {
            new Ip6Address("1F:A2:FF21:BCD4:AAAA:1234:RF32:1121:CD64");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip6Address, invalid number of blocks", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenContainsInvalidCharacters() {
        try {
            new Ip6Address("t:A2:rb:!:?23:1234:RF32:1121");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip6Address, blocks must be a number in the range 0 - FFFF", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenBlockOutOfRange() {
        try {
            new Ip6Address("FFFF:FFFF:FFFF:FFFF1:FFFF:FFFF:FFFF:FFFF");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip6Address, blocks must be a number in the range 0 - FFFF", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenMultipleCompacted() {
        try {
            new Ip6Address("::ABCD::1");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip6Address, must contain only one compacted section", e.getMessage());
        }
    }

    @Test
    public void comapactsLongestZeros() throws InvalidIpAddressException {
        addr = new Ip6Address("0:0:0:0:0:0:0:0");
        assertEquals("::", addr.toAddressString());

        addr = new Ip6Address("0:0:0:0:0:0:0:1");
        assertEquals("::1", addr.toAddressString());

        addr = new Ip6Address("0:0:0:0:abc1:211:4FD:1");
        assertEquals("::abc1:211:4fd:1", addr.toAddressString());

        addr = new Ip6Address("0:0:12A:665:0:0:0:1");
        assertEquals("0:0:12a:665::1", addr.toAddressString());

        addr = new Ip6Address("0:0:0:12A:665:0:0:1");
        assertEquals("::12a:665:0:0:1", addr.toAddressString());

        addr = new Ip6Address("0:0:0:12A:0:0:AA:0");
        assertEquals("::12a:0:0:aa:0", addr.toAddressString());

        addr = new Ip6Address("12:AAB:12A:665:0:0:0:0");
        assertEquals("12:aab:12a:665::", addr.toAddressString());

        addr = new Ip6Address("00:000:0000:12A:00:000:AA:0");
        assertEquals("::12a:0:0:aa:0", addr.toAddressString());

        addr = new Ip6Address("0:0:0:12A:0:0:AA:0");
        assertEquals("::12a:0:0:aa:0", addr.toAddressString());
    }

    @Test
    public void doesNotCompactSingleZeros() throws InvalidIpAddressException {
        addr = new Ip6Address("1A:FFff:Ff10:0:ABCD:Ed12:ED1:1");
        assertEquals("1a:ffff:ff10:0:abcd:ed12:ed1:1", addr.toAddressString());
    }

    @Test
    public void acceptsValidBlocks() throws InvalidIpAddressException {
        addr = new Ip6Address("1:1:1:1:1:1:1:1");
        assertEquals("1:1:1:1:1:1:1:1", addr.toAddressString());

        addr = new Ip6Address("FFFF:ffff:FFFF:FFFF:FFFF:FFFF:FFFF:ffff");
        assertEquals("ffff:ffff:ffff:ffff:ffff:ffff:ffff:ffff", addr.toAddressString());
    }

    @Test
    public void stripsLeadingZeros() throws InvalidIpAddressException {
        addr = new Ip6Address("001A:FFff:Ff10:0000:ABCD:Ed12:0ED1:0001");
        assertEquals("1a:ffff:ff10:0:abcd:ed12:ed1:1", addr.toAddressString());
    }

    @Test
    public void acceptsCompactForm() throws InvalidIpAddressException {
        addr = new Ip6Address("::");
        assertEquals("::", addr.toAddressString());

        addr = new Ip6Address("::ABCD:Ed12:0ED1:0001");
        assertEquals("::abcd:ed12:ed1:1", addr.toAddressString());

        addr = new Ip6Address("::1");
        assertEquals("::1", addr.toAddressString());

        addr = new Ip6Address("ABCD:Ed12:0ED1:0001::");
        assertEquals("abcd:ed12:ed1:1::", addr.toAddressString());

        addr = new Ip6Address("1::");
        assertEquals("1::", addr.toAddressString());

        addr = new Ip6Address("ABCD:Ed12::0ED1:0001");
        assertEquals("abcd:ed12::ed1:1", addr.toAddressString());

        addr = new Ip6Address("1::1");
        assertEquals("1::1", addr.toAddressString());
    }

    @Test
    public void convertsToBinary() throws InvalidIpAddressException {
        addr = new Ip6Address("0:0:0:0:0:0:0:0");
        assertEquals(
            "0000000000000000 0000000000000000 0000000000000000 0000000000000000 " +
            "0000000000000000 0000000000000000 0000000000000000 0000000000000000", addr.toBinaryString());

        addr = new Ip6Address("5f05:2000:80ad:5800:58:800:2023:1d71");
        assertEquals(
            "0101111100000101 0010000000000000 1000000010101101 0101100000000000 " +
            "0000000001011000 0000100000000000 0010000000100011 0001110101110001", addr.toBinaryString());

        addr = new Ip6Address("0:0:0:0:0:0:0:1");
        assertEquals(
            "0000000000000000 0000000000000000 0000000000000000 0000000000000000 " +
                "0000000000000000 0000000000000000 0000000000000000 0000000000000001", addr.toBinaryString());
    }

    @Test
    public void converstToIp4Compatible() throws InvalidIpAddressException, Ip4ComaptibleConversionException {
        addr = new Ip6Address("::0102:f001");
        assertEquals("::1.2.240.1", addr.toIp4Compatible());

        addr = new Ip6Address("::f001");
        assertEquals("::0.0.240.1", addr.toIp4Compatible());

        addr = new Ip6Address("::0001");
        assertEquals("::0.0.0.1", addr.toIp4Compatible());
    }

    @Test(expected = Ip4ComaptibleConversionException.class)
    public void throwsExceptionWhenNotIp4Compatible() throws InvalidIpAddressException, Ip4ComaptibleConversionException {
        addr = new Ip6Address("::0102:f001:12aa");
        addr.toIp4Compatible();
    }
}