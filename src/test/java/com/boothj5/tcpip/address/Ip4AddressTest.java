package com.boothj5.tcpip.address;

import com.boothj5.tcpip.exception.InvalidIpAddressException;
import com.boothj5.tcpip.ip4class.Unicast;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.fail;

public class Ip4AddressTest {

    private Ip4Address addr;
    private Unicast addrClass;

    @Test
    public void defaultsToZeroQuads() {
        addr = new Ip4Address();
        assertEquals("0.0.0.0", addr.toAddressString());
    }

    @Test
    public void throwsExceptionWhenNull() {
        try {
            new Ip4Address(null);
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip4Address, cannot be null or empty String", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenEmpty() {
        try {
            new Ip4Address("");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip4Address, cannot be null or empty String", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenWhitespace() {
        try {
            new Ip4Address("  ");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip4Address, cannot be null or empty String", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenOneQuad() {
        try {
            new Ip4Address("123");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip4Address, must contain 4 quads", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenInvalidSeparator() {
        try {
            new Ip4Address("192.168,10.1");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip4Address, must contain 4 quads", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenTwoManyQuads() {
        try {
            new Ip4Address("192.168.10.1.12");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip4Address, must contain 4 quads", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenContainsInvalidCharacters() {
        try {
            new Ip4Address("192.b.10.1");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip4Address, quads must be a number in the range 0 - 255", e.getMessage());
        }
    }

    @Test
    public void throwsExceptionWhenQuadOutOfRange() {
        try {
            new Ip4Address("255.256.255.255");
            fail("Exception InvalidIpAddressException not thrown");
        } catch (InvalidIpAddressException e) {
            assertEquals("Invalid Ip4Address, quads must be a number in the range 0 - 255", e.getMessage());
        }
    }

    @Test
    public void acceptsValidQuads() throws InvalidIpAddressException {
        addr = new Ip4Address("0.0.0.0");
        assertEquals("0.0.0.0", addr.toAddressString());

        addr = new Ip4Address("1.1.1.1");
        assertEquals("1.1.1.1", addr.toAddressString());

        addr = new Ip4Address("255.255.255.255");
        assertEquals("255.255.255.255", addr.toAddressString());

        addr = new Ip4Address("192.168.12.17");
        assertEquals("192.168.12.17", addr.toAddressString());
    }

    @Test
    public void convertsToBinary() throws InvalidIpAddressException {
        addr = new Ip4Address("0.0.0.0");
        assertEquals("00000000 00000000 00000000 00000000", addr.toBinaryString());

        addr = new Ip4Address("1.2.3.4");
        assertEquals("00000001 00000010 00000011 00000100", addr.toBinaryString());

        addr = new Ip4Address("0.0.0.255");
        assertEquals("00000000 00000000 00000000 11111111", addr.toBinaryString());

        addr = new Ip4Address("165.195.130.107");
        assertEquals("10100101 11000011 10000010 01101011", addr.toBinaryString());

        addr = new Ip4Address("255.255.255.255");
        assertEquals("11111111 11111111 11111111 11111111", addr.toBinaryString());
    }
}