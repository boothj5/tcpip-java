package com.boothj5.tcpip.address;

import com.boothj5.tcpip.exception.InvalidIpAddressException;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class Ip4MappedIp6AddressTest {

    private Ip4MappedIp6Address addr;

    @Test (expected = InvalidIpAddressException.class)
    public void usesIp4Validation() throws InvalidIpAddressException {
        new Ip4MappedIp6Address(new Ip4Address("192.166.aa.1"));
    }

    @Test
    public void acceptsValidIp4Address() throws InvalidIpAddressException {
        addr = new Ip4MappedIp6Address(new Ip4Address("192.168.10.1"));
        assertEquals("::ffff:192.168.10.1", addr.toAddressString());
    }

    @Test
    public void convertsToBinary() throws InvalidIpAddressException {
        addr = new Ip4MappedIp6Address(new Ip4Address("165.195.130.107"));
        assertEquals("0000000000000000 0000000000000000 0000000000000000 0000000000000000 0000000000000000 " +
            "1111111111111111 10100101 11000011 10000010 01101011", addr.toBinaryString());
    }
}