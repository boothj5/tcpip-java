package com.boothj5.tcpip.ip4class;

import com.boothj5.tcpip.address.Ip4Address;
import com.boothj5.tcpip.exception.InvalidIpAddressException;
import com.boothj5.tcpip.exception.InvalidUnicastAddress;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class UnicastCTest {

    private Ip4Address addr;
    private UnicastC unicast;

    @Test(expected = InvalidUnicastAddress.class)
    public void throwsExceptionWhenNotUnicastC() throws InvalidIpAddressException, InvalidUnicastAddress {
        addr = new Ip4Address("255.0.0.0");
        new UnicastC(addr);
    }

    @Test
    public void returnsUnicastClassC() throws InvalidIpAddressException, InvalidUnicastAddress {
        addr = new Ip4Address("192.1.122.1");
        unicast = new UnicastC(addr);
        assertEquals("192.1.122", unicast.getNet());
        assertEquals("1", unicast.getHost());

        addr = new Ip4Address("223.255.1.23");
        unicast = new UnicastC(addr);
        assertEquals("223.255.1", unicast.getNet());
        assertEquals("23", unicast.getHost());

        addr = new Ip4Address("200.0.0.0");
        unicast = new UnicastC(addr);
        assertEquals("200.0.0", unicast.getNet());
        assertEquals("0", unicast.getHost());
    }
}