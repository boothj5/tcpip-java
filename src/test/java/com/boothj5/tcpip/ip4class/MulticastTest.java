package com.boothj5.tcpip.ip4class;

import com.boothj5.tcpip.address.Ip4Address;
import com.boothj5.tcpip.exception.InvalidIpAddressException;
import com.boothj5.tcpip.exception.InvalidMulticastAddress;
import org.junit.Test;

public class MulticastTest {
    private Ip4Address addr;

    @Test(expected = InvalidMulticastAddress.class)
    public void throwsExceptionWhenNotMulticast() throws InvalidIpAddressException, InvalidMulticastAddress {
        addr = new Ip4Address("192.2.33.122");
        new Multicast(addr);
    }

    @Test
    public void createsMulticastAddress() throws InvalidIpAddressException, InvalidMulticastAddress {
        addr = new Ip4Address("239.2.33.122");
        new Multicast(addr);
    }
}