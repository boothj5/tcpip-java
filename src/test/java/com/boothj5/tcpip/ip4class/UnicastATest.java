package com.boothj5.tcpip.ip4class;

import com.boothj5.tcpip.address.Ip4Address;
import com.boothj5.tcpip.exception.InvalidIpAddressException;
import com.boothj5.tcpip.exception.InvalidUnicastAddress;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class UnicastATest {

    private Ip4Address addr;
    private UnicastA unicast;

    @Test(expected = InvalidUnicastAddress.class)
    public void throwsExceptionWhenNotUnicastA() throws InvalidIpAddressException, InvalidUnicastAddress {
        addr = new Ip4Address("224.0.0.0");
        new UnicastA(addr);
    }

    @Test
    public void returnsUnicastClassA() throws InvalidIpAddressException, InvalidUnicastAddress {
        addr = new Ip4Address("0.0.0.0");
        unicast = new UnicastA(addr);
        assertEquals("0", unicast.getNet());
        assertEquals("0.0.0", unicast.getHost());

        addr = new Ip4Address("0.255.1.23");
        unicast = new UnicastA(addr);
        assertEquals("0", unicast.getNet());
        assertEquals("255.1.23", unicast.getHost());

        addr = new Ip4Address("11.255.1.23");
        unicast = new UnicastA(addr);
        assertEquals("11", unicast.getNet());
        assertEquals("255.1.23", unicast.getHost());

        addr = new Ip4Address("127.255.1.23");
        unicast = new UnicastA(addr);
        assertEquals("127", unicast.getNet());
        assertEquals("255.1.23", unicast.getHost());
    }
}