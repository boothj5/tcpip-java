package com.boothj5.tcpip.ip4class;

import com.boothj5.tcpip.address.Ip4Address;
import com.boothj5.tcpip.exception.InvalidIpAddressException;
import com.boothj5.tcpip.exception.InvalidUnicastAddress;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class UnicastBTest {

    private Ip4Address addr;
    private UnicastB unicast;

    @Test(expected = InvalidUnicastAddress.class)
    public void throwsExceptionWhenNotUnicastB() throws InvalidIpAddressException, InvalidUnicastAddress {
        addr = new Ip4Address("100.0.0.0");
        new UnicastB(addr);
    }

    @Test
    public void returnsUnicastClassB() throws InvalidIpAddressException, InvalidUnicastAddress {
        addr = new Ip4Address("182.1.182.1");
        unicast = new UnicastB(addr);
        assertEquals("182.1", unicast.getNet());
        assertEquals("182.1", unicast.getHost());

        addr = new Ip4Address("128.255.1.23");
        unicast = new UnicastB(addr);
        assertEquals("128.255", unicast.getNet());
        assertEquals("1.23", unicast.getHost());

        addr = new Ip4Address("191.255.1.23");
        unicast = new UnicastB(addr);
        assertEquals("191.255", unicast.getNet());
        assertEquals("1.23", unicast.getHost());
    }
}