package com.boothj5.tcpip.ip4class;

import com.boothj5.tcpip.address.Ip4Address;
import com.boothj5.tcpip.exception.InvalidIpAddressException;
import com.boothj5.tcpip.exception.InvalidReservedAddress;
import org.junit.Test;

public class ReservedTest {

    private Ip4Address addr;

    @Test(expected = InvalidReservedAddress.class)
    public void throwsExceptionWhenNotReserved() throws InvalidIpAddressException, InvalidReservedAddress {
        addr = new Ip4Address("192.2.33.122");
        new Reserved(addr);
    }

    @Test
    public void createsReservedAddress() throws InvalidIpAddressException, InvalidReservedAddress {
        addr = new Ip4Address("255.2.33.122");
        new Reserved(addr);
    }
}