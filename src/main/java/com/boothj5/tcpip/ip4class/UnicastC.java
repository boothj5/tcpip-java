package com.boothj5.tcpip.ip4class;

import com.boothj5.tcpip.BinUtils;
import com.boothj5.tcpip.address.Ip4Address;
import com.boothj5.tcpip.address.IpAddress;
import com.boothj5.tcpip.exception.InvalidUnicastAddress;

public class UnicastC implements IpAddress, Unicast {

    private final Ip4Address addr;

    public UnicastC(Ip4Address addr) throws InvalidUnicastAddress {
        String quad1Bin = BinUtils.intToBin(8, addr.getQuad1());
        if (!quad1Bin.startsWith("110")) {
            throw new InvalidUnicastAddress();
        }

        this.addr = addr;
    }

    @Override
    public String toAddressString() {
        return addr.toAddressString();
    }

    @Override
    public String toBinaryString() {
        return addr.toBinaryString();
    }

    @Override
    public String getNet() {
        return addr.getQuad1() + "." + addr.getQuad2() + "." + addr.getQuad3();
    }

    @Override
    public String getHost() {
        return Integer.toString(addr.getQuad4());
    }
}
