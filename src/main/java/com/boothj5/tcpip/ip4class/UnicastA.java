package com.boothj5.tcpip.ip4class;

import com.boothj5.tcpip.BinUtils;
import com.boothj5.tcpip.address.Ip4Address;
import com.boothj5.tcpip.address.IpAddress;
import com.boothj5.tcpip.exception.InvalidUnicastAddress;

public class UnicastA implements IpAddress, Unicast {

    private final Ip4Address addr;

    public UnicastA(Ip4Address addr) throws InvalidUnicastAddress {
        String quad1Bin = BinUtils.intToBin(8, addr.getQuad1());
        if (!quad1Bin.startsWith("0")) {
            throw new InvalidUnicastAddress();
        }

        this.addr = addr;
    }

    @Override
    public String toAddressString() {
        return addr.toAddressString();
    }

    @Override
    public String toBinaryString() {
        return addr.toBinaryString();
    }

    @Override
    public String getNet() {
        return Integer.toString(addr.getQuad1());
    }

    @Override
    public String getHost() {
        return addr.getQuad2() + "." + addr.getQuad3() + "." + addr.getQuad4();
    }
}
