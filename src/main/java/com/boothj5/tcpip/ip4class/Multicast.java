package com.boothj5.tcpip.ip4class;

import com.boothj5.tcpip.BinUtils;
import com.boothj5.tcpip.address.Ip4Address;
import com.boothj5.tcpip.address.IpAddress;
import com.boothj5.tcpip.exception.InvalidMulticastAddress;

public class Multicast implements IpAddress {

    private final Ip4Address addr;

    public Multicast(Ip4Address addr) throws InvalidMulticastAddress {
        String quad1Bin = BinUtils.intToBin(8, addr.getQuad1());
        if (!quad1Bin.startsWith("1110")) {
            throw new InvalidMulticastAddress();
        }

        this.addr = addr;
    }


    @Override
    public String toAddressString() {
        return addr.toAddressString();
    }

    @Override
    public String toBinaryString() {
        return addr.toBinaryString();
    }
}
