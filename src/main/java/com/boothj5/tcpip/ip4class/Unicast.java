package com.boothj5.tcpip.ip4class;

public interface Unicast {
    String getNet();
    String getHost();
}
