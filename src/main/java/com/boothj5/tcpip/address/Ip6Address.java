package com.boothj5.tcpip.address;

import com.boothj5.tcpip.BinUtils;
import com.boothj5.tcpip.exception.InvalidIpAddressException;
import com.boothj5.tcpip.exception.Ip4ComaptibleConversionException;

public class Ip6Address implements IpAddress {

    private static final String NULL_OR_EMPTY_ERROR = "Invalid Ip6Address, cannot be null or empty String";
    private static final String BLOCK_FORMAT_ERROR = "Invalid Ip6Address, blocks must be a number in the range 0 - FFFF";
    private static final String BLOCK_COUNT_ERROR = "Invalid Ip6Address, invalid number of blocks";

    private final int[] blocks = new int[8];

    public Ip6Address() { }

    public Ip6Address(String addr) throws InvalidIpAddressException {
        if (addr == null || "".equals(addr.trim())) {
            throw new InvalidIpAddressException(NULL_OR_EMPTY_ERROR);
        }

        String addrLower = addr.toLowerCase();

        if (addrLower.contains("::")) {
            String[] compactStrings = addrLower.split("::");
            if (compactStrings.length > 2) {
                throw new InvalidIpAddressException("Invalid Ip6Address, must contain only one compacted section");
            }

            if (addrLower.equals("::")) {
                return;
            }

            if (addrLower.endsWith("::")) {
                setLeadingBlocks(compactStrings[0]);
                return;
            }

            if (addrLower.startsWith("::")) {
                setTrailingBlocks(compactStrings[1]);
                return;
            }

            setLeadingBlocks(compactStrings[0]);
            setTrailingBlocks(compactStrings[1]);

            return;
        }

        String[] blockStrings = parseBlockStrings(addrLower, 8, 8);
        for (int i = 0; i < 8; i++) {
            blocks[i] = Integer.parseInt(blockStrings[i], 16);
        }
    }

    @Override
    public String toAddressString() {
        return blocksToString(blocks);
    }

    @Override
    public String toBinaryString() {
        return BinUtils.intToBin(16, blocks[0])+ " " +
            BinUtils.intToBin(16, blocks[1]) + " " +
            BinUtils.intToBin(16, blocks[2]) + " " +
            BinUtils.intToBin(16, blocks[3]) + " " +
            BinUtils.intToBin(16, blocks[4]) + " " +
            BinUtils.intToBin(16, blocks[5]) + " " +
            BinUtils.intToBin(16, blocks[6]) + " " +
            BinUtils.intToBin(16, blocks[7]);
    }

    private void setLeadingBlocks(String compactString) throws InvalidIpAddressException {
        String[] leadingBlockStrings = parseBlockStrings(compactString, 1, 7);
        for (int i = 0; i < leadingBlockStrings.length; i++) {
            blocks[i] = Integer.parseInt(leadingBlockStrings[i], 16);
        }
    }

    private void setTrailingBlocks(String compactString) throws InvalidIpAddressException {
        String[] trailingBlockStrings = parseBlockStrings(compactString, 1, 7);
        for (int i = 0; i < trailingBlockStrings.length; i++) {
            blocks[8 - trailingBlockStrings.length + i] = Integer.parseInt(trailingBlockStrings[i], 16);
        }
    }

    private String[] parseBlockStrings(String addrLower, int min, int max) throws InvalidIpAddressException {
        String[] blockStrings = addrLower.split(":");
        if (blockStrings.length < min || blockStrings.length > max) {
            throw new InvalidIpAddressException(BLOCK_COUNT_ERROR);
        }

        for (String blockString : blockStrings) {
            if (blockString.length() < 1 || blockString.length() > 4) {
                throw new InvalidIpAddressException(BLOCK_FORMAT_ERROR);
            }

            for (char c : blockString.toCharArray()) {
                if (!isHexDigit(c)) {
                    throw new InvalidIpAddressException(BLOCK_FORMAT_ERROR);
                }
            }
        }
        return blockStrings;
    }

    private static String blocksToString(int blocks[]) {
        String result = "";

        boolean inRepeat = false;
        boolean needsCompact = false;
        int pos = -1;
        int maxPos = -1;
        int len = 0;
        int maxLen = 0;

        for (int i = 1; i < 8; i++) {
            if (blocks[i-1] == 0 && blocks[i] == 0) {
                if (!inRepeat) {
                    inRepeat = true;
                    needsCompact = true;
                    pos = i-1;
                    len += 2;
                } else {
                    len++;
                }
            } else {
                if (inRepeat && len > maxLen) {
                    maxPos = pos;
                    maxLen = len;
                    len = 0;
                    inRepeat = false;
                }
            }
        }
        if (inRepeat && len > maxLen) {
            maxPos = pos;
            maxLen = len;
        }

        inRepeat = false;
        int compactCount = 0;
        if (needsCompact) {
            for (int i = 0; i < 8; i++) {
                if (inRepeat) {
                    compactCount++;
                    if (compactCount == maxLen) {
                        inRepeat = false;
                    }
                } else if (i == maxPos) {
                    result += "::";
                    inRepeat = true;
                    compactCount++;
                } else {
                    result += Integer.toHexString(blocks[i]);
                    if (i < 7 && i + 1 != maxPos) {
                        result += ":";
                    }
                }
            }

            return result;
        }

        for (int i = 0; i < 8; i++) {
            result += Integer.toHexString(blocks[i]);
            if (i < 7) {
                result += ":";
            }
        }

        return result;
    }

    private static boolean isHexDigit(char c) {
        if (Character.isDigit(c)) {
            return true;
        }
        if (c == 'a' || c == 'b' || c == 'c' || c == 'd' || c == 'e' || c == 'f') {
            return true;
        }
        return false;
    }

    public String toIp4Compatible() throws Ip4ComaptibleConversionException {
        if (blocks[0] != 0 || blocks[1] != 0 || blocks[2] != 0 ||
                blocks[3] != 0 || blocks[4] != 0 || blocks[5] != 0) {
            throw new Ip4ComaptibleConversionException();
        }

        String block7bin = BinUtils.intToBin(16, blocks[6]);
        String block8bin = BinUtils.intToBin(16, blocks[7]);

        String quad1String = block7bin.substring(0, 8);
        String quad2String = block7bin.substring(8, 16);
        String quad3String = block8bin.substring(0, 8);
        String quad4String = block8bin.substring(8, 16);

        return "::" +
            Integer.parseInt(quad1String, 2) + "." +
            Integer.parseInt(quad2String, 2) + "." +
            Integer.parseInt(quad3String, 2) + "." +
            Integer.parseInt(quad4String, 2);
    }
}
