package com.boothj5.tcpip.address;

public interface IpAddress {

    String toAddressString();

    String toBinaryString();
}
