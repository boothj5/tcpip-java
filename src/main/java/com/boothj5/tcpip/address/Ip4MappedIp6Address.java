package com.boothj5.tcpip.address;

import com.boothj5.tcpip.exception.InvalidIpAddressException;

public class Ip4MappedIp6Address implements IpAddress {

    private final Ip4Address ip4Address;

    public Ip4MappedIp6Address(Ip4Address ip4Address) throws InvalidIpAddressException {
        this.ip4Address = ip4Address;
    }

    @Override
    public String toAddressString() {
        return "::ffff:" + ip4Address.toAddressString();
    }

    @Override
    public String toBinaryString() {
        return "0000000000000000 0000000000000000 0000000000000000 0000000000000000 0000000000000000 " +
            "1111111111111111 " + ip4Address.toBinaryString();
    }
}
