package com.boothj5.tcpip.address;

import com.boothj5.tcpip.BinUtils;
import com.boothj5.tcpip.exception.InvalidIpAddressException;

public class Ip4Address implements IpAddress {

    private static final String NULL_OR_EMPTY_ERROR = "Invalid Ip4Address, cannot be null or empty String";
    private static final String QUAD_FORMAT_ERROR = "Invalid Ip4Address, quads must be a number in the range 0 - 255";
    private static final String QUAD_COUNT_ERROR = "Invalid Ip4Address, must contain 4 quads";

    private final String addr;
    private final int[] quads = new int[4];

    public Ip4Address() {
        addr = "0.0.0.0";
    }

    public Ip4Address(String addr) throws InvalidIpAddressException {
        if (addr == null || "".equals(addr.trim())) {
            throw new InvalidIpAddressException(NULL_OR_EMPTY_ERROR);
        }

        String[] quadStrings = addr.split("\\.");
        if (quadStrings.length != 4) {
            throw new InvalidIpAddressException(QUAD_COUNT_ERROR);
        }

        for (String quadString : quadStrings) {
            for (char c : quadString.toCharArray()) {
                if (!Character.isDigit(c)) {
                    throw new InvalidIpAddressException(QUAD_FORMAT_ERROR);
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            quads[i] = Integer.parseInt(quadStrings[i]);
            if (quads[i] < 0 || quads[i] > 255) {
                throw new InvalidIpAddressException(QUAD_FORMAT_ERROR);
            }
        }

        this.addr = addr;
    }

    @Override
    public String toAddressString() {
        return addr;
    }

    @Override
    public String toBinaryString() {
        return BinUtils.intToBin(8, quads[0]) + " " +
            BinUtils.intToBin(8, quads[1]) + " " +
            BinUtils.intToBin(8, quads[2]) + " " +
            BinUtils.intToBin(8, quads[3]);
    }

    public int getQuad1() {
        return quads[0];
    }

    public int getQuad2() {
        return quads[1];
    }

    public int getQuad3() {
        return quads[2];
    }

    public int getQuad4() {
        return quads[3];
    }
}
