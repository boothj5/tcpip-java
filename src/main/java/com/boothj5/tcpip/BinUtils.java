package com.boothj5.tcpip;

public class BinUtils {
    public static String intToBin(int bits, int i) {
        return String.format("%1$" + bits + "s", Integer.toBinaryString(i)).replace(" ", "0");
    }
}
