package com.boothj5.tcpip.exception;

public class InvalidIpAddressException extends Throwable {
    private InvalidIpAddressException() {}

    public InvalidIpAddressException(String message) {
        super(message);
    }
}
